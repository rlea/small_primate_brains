%% LOGnorm tract surface

function lognorm_surf_tract(main_folder,specie,hemi)

switch hemi
    case 'l'
        Hemi='L';
    otherwise
        Hemi='R';
end

resultsDir = [main_folder '/' specie '/tractography/average/surface/'];
tractSurf = readimgfile([resultsDir '/tracts_surf_smooth.' hemi '.dtseries.nii'], Hemi);
tractSurf_lognorm = zeros(size(tractSurf));
tractSurf_lognorm_thr = zeros(size(tractSurf));
for k = 1:size(tractSurf,2)
    tractSurf_lognorm(:,k)= log(tractSurf(:,k))/max(log(tractSurf(:,k)));
end

for n = 1:size(tractSurf,2)
    tractSurf_lognorm_thr(tractSurf_lognorm(:,n)<0.8,n)=0; %threshold
    tractSurf_lognorm_thr(tractSurf_lognorm(:,n)>=0.8,n)=n; %binarize and give a value 1 2 3 4 5... for each tracts which is going to match the labels
end
saveimgfile(tractSurf_lognorm_thr,[resultsDir '/tracts_surf_smooth_lognorm_thr08.' hemi '.dtseries.nii'], Hemi);

