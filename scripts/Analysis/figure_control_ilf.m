%% ILF verification
% We seeded the tractography with a large mask in parietal and used a target mask 
% in the anterior temporal lobe either in the territory of MdLF or ILF.
% This scripts computes the ratio of streamlines surviving these two tractography protocols, 
% using the waytotal file created during tractography.

main_folder = '/your_path';
species = {'lemur','squirrel_monkey','macaque'};

tracts = {'mdlftest','ilftest'}; %order matter for ilf/mdlf
hemis={'l','r'};

% initialization
WTdata = zeros(3*length(hemis),length(species)*length(tracts));
jt=0;
for ss=1:length(species)
    specie = species{ss};
        %get the names of the different folder
    Namepath = strcat(main_folder, specie, '/tractography/tracts/');
    Namedir = dir(Namepath);
    isub = [Namedir(:).isdir]; %# returns logical vector
    nameFolds = {Namedir(isub).name}';
    nameFolds(ismember(nameFolds,{'.','..'})) = [];
    for tt = 1:length(tracts)
        tract = tracts{tt};
        jt=jt+1;
        it=0;
        for h = 1:length(hemis)
           hemi=hemis{h}; 
           for nn=1:length(nameFolds)
                name=nameFolds{nn};
                it=it+1;
                WTfile = strcat(Namepath,name,'/tracts/',tract,'_',hemi,'/waytotal');
                WT = importdata(WTfile);
                WTdata(it,jt) = WT;
            end
   
        end
    end
end

%% ratio
ratioData = zeros (length(WTdata),1);
sp = 0;
for z = 1:2:size(WTdata,2)
    sp = sp+1;
    for y = 1:length(WTdata)
        ratioData(y,sp)=WTdata(y,z)/WTdata(y,z+1);
    end
end

%% plot

yline(1,'Color',[0.25 0.25 0.25],'LineWidth',1.5); %line at 1
ylabel('mdlf/ilf (log)','FontSize',10) 
hold on
b = boxplot(ratioData,'OutlierSize',5,'Symbol','+','colors',[0.25  0.25 0.25],'Widths',0.3);
set(b, 'LineWidth',1.5)
set(gca,'XTickLabel',{'Lemur','Squirrel monkey','Macaque'},'FontSize',10);
ax = gca;
ax.YAxis.Scale ="log";
maxy=max(max(ratioData))+10;
miny=min(min(ratioData));
ylim([miny maxy])
yticks([miny 0.1 1 10])
yticklabels({int2str(miny),'0.1','1','10'})

%get outliers as defined by boxplot, actually there's no outliers here
%that we get mdlf/ilf instead of ilf/mdlf
[q1,q2,q3,w0,w1,outliers] = boxplot_statistics(ratioData);
outliersd=outliers(:);
x = [0.75:0.1:1.25,1.75:0.1:2.25,2.75:0.1:3.25]; x = x(:); %to plot the outliers at a good distance
xd = ratioData(:);

outx=nonzeros(outliersd.*x); outliersd=nonzeros(outliersd.*xd);

scatter(x,xd,30,'MarkerEdgeColor',[0.75 0.75 0.75],'MarkerFaceColor', [0.75 0.75 0.75],'LineWidth',1) %plot individuals
scatter(outx,outliersd,40,'+','MarkerEdgeColor',[0.68  0.5 0],'LineWidth',1.5) %plot outliers
set(gcf,'units', 'centimeters','position',[0 0 9 6],'color','w')
hold off

figname = [main_folder '/figures/tractography/control_ilf.png'];
saveas(gcf,figname)



