%% Average fdt_matrix2

%% ----------------
% edit with your own path
main_folder =  '/your_path';
%% ----------------

subjects = {'01','02','03'};

%% Loop trough species, hemispheres
species = {'lemur','squirrel_monkey','macaque'};

hemis = {'l', 'r'};

for s = 1:length(species) % Loop trough species
    specie = species{s};
    resultsDir = [main_folder specie '/blueprint/'];

    for h = 1:length(hemis) % Loop trough hemis
        hemi = hemis{h};
        avgmat=0;
        for subj = 1:length(subjects) % loop trough subjects
            subject = subjects{subj};
            disp(['-------' subject '--------'])
            fname=[resultsDir '/subjects/' subject '/surf_seed_' hemi '/fdt_matrix2.dot'];
            mat=spconvert(load(fname));
            avgmat = avgmat+mat;
            
        end %subj
        avgmat=avgmat/length(subjects);
        save([resultsDir '/AVG_' specie '_Matrix_cortexbrain_' hemi '.mat'],'avgmat','-v7.3');
        
    end %hemi
end % species
