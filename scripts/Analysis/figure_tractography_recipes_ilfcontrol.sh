#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files
set -e    # stop immediately on error


main_root=/your_path/

species="lemur squirrel_monkey macaque"

tracts="ilftest mdlftest"

h="l"
# hemispere (choose only one, it will look better)

# directory to store the figures
mkdir -p $main_root/figures/recipes

masks="seed target"

for t in $tracts; do

	for m in $masks; do
		files=""

		for sp in $species; do

			if [[ "$sp" = "lemur" ]] ; then
				zoom=830
				xcentreY=0
				xcentreZ=0			
				ycentreX=0
				ycentreZ=0
				zcentreX=0.05
				zcentreY=0
				crop=0
			elif [[ "$sp" = "squirrel_monkey" ]] ; then
				zoom=800
				xcentreY=-0.1
				xcentreZ=0.05
				ycentreX=-0.05
				ycentreZ=0.07
				zcentreX=0
				zcentreY=-0.1
				crop=0
			elif [[ "$sp" = "macaque" ]] ; then
				zoom=470
				xcentreY=0
				xcentreZ=0
				ycentreX=-0.05
				ycentreZ=0
				zcentreX=0
				zcentreY=0			
				crop=0
			fi



			outfile=$main_root/figures/recipes/${t}_${sp}_${m}.pdf
			fsleyescommand="render --scene ortho --outfile $outfile --size 1499 500 -c $crop -hc -p 3 -hd -hl --xzoom $zoom --yzoom $zoom --zzoom $zoom "
			# hc: hide cursor, p: performance (3=best looking), -hd: better display on mac, -hl: hide labels, -c border
			
			# get the cursor on middle voxel of the seed
			Cs=`fslstats $main_root/$sp/tractography/protocols/${t}_${h}/$m -C`
			XYZ=""
			for c in $Cs; do
				XYZ+="${c%%.*} "
			done

			fsleyescommand+="--voxelLoc $XYZ --xcentre $xcentreY $xcentreZ --ycentre $ycentreX $ycentreZ --zcentre $zcentreX $zcentreY "

			fsleyescommand+="$main_root/$sp/standard/t2_template.nii.gz "

				# recipes folder
			recipes_root=$main_root/$sp/tractography/protocols/${t}_${h}

			if [[ "$m" = "target" ]] ; then
				if [[ "$t" = "mdlftest" ]] ; then
					mycol=blue
				elif [[ "$t" = "ilftest" ]] ; then
					mycol=yellow
				fi

			elif [[ "$m" = "seed" ]] ; then
				mycol=red
			fi

		
			fsleyescommand+="$recipes_root/$m -cm $mycol $recipes_root/exclude -cm Greyscale "

			fsleyes $fsleyescommand

			files+="$outfile "

		done
		convert $files -append $main_root/figures/recipes/recipe_${t}_${m}.png #Need imagemagick
		rm $files
	done
done

