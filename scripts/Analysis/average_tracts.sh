#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files
set -e    # stop immediately on error

main_root=/your_path/

species="lemur squirrel_monkey macaque"

tracts="cbd cbp cbt ilf lft mdlf or slfc unc"

hemis="l r"

for sp in $species; do
	# Tracto folder
	tracto_root=$main_root/$sp/tractography
	for t in $tracts; do
		for h in $hemis; do
			for sub_dir in $tracto_root/tracts/*/; do
				#normalize by waytotal
				if [[ "$t" = "ilf_test" ]] || [[ "$t" = "mdlf_test" ]]; then # because no invert
					cp $sub_dir/tracts/${t}_${h}/densityNorm.nii.gz $sub_dir/tracts/${t}_${h}/sum_densityNorm.nii.gz
				else
					fslmaths $sub_dir/tracts/${t}_${h}/sum_density -div `cat $sub_dir/tracts/${t}_${h}/sum_waytotal` $sub_dir/tracts/${t}_${h}/sum_densityNorm
				fi
			done

			mkdir -p $tracto_root/average/${t}
			mkdir -p $tracto_root/average/all

			#average tract
			fsladd $tracto_root/average/$t/${t}_${h}.nii.gz -m $tracto_root/tracts/*/tracts/${t}_${h}/sum_densityNorm.nii.gz
			
			# log transform
			fslmaths $tracto_root/average/$t/${t}_${h}.nii.gz -mul 100000000000 -add 1 $tracto_root/average/$t/${t}_${h}_mul
			fslmaths $tracto_root/average/$t/${t}_${h}_mul -log $tracto_root/average/$t/${t}_${h}_log
			fslmaths $tracto_root/average/$t/${t}_${h}_log  -div `fslstats $tracto_root/average/$t/${t}_${h}_log -R | awk '{print $2}'` $tracto_root/average/$t/${t}_${h}_lognorm
			
			rm $tracto_root/average/$t/${t}_${h}_mul.nii.gz
			rm $tracto_root/average/$t/${t}_${h}_log.nii.gz
			if [[ "$t" = "ilf_test" ]] || [[ "$t" = "mdlf_test" ]]; then # because no need to copy
				echo "done $t"
			else	
				cp $tracto_root/average/$t/${t}_${h}.nii.gz $tracto_root/average/all/${t}_${h}.nii.gz
				cp $tracto_root/average/$t/${t}_${h}_lognorm.nii.gz $tracto_root/average/all/${t}_${h}_lognorm.nii.gz
				echo "done $t"
			fi

		done

	done
done
